#include <stdio.h>

// -Wall would complain about unused parameters and -Werror would make it into an error
// But since they were disabled, this code compiles with no issues
int main(int argc, char **argv)
{
	printf("Hello world!\n");
}
