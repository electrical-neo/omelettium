#include <stdio.h>
#define __OMELETTIUM_H_IMPL
#include "../../omelettium.h"

int main(int argc, char **argv)
{
	omelettium_init(argc, argv);

	struct omelettium_compilation exe = {0};
	exe.name = "custom-compilation.exe";
	omelettium_compilation_append_src(&exe, "src/main.c");
	exe.disable_werror = true;
	exe.disable_wall = true;
	exe.disable_wextra = true;
	exe.disable_pedantic = true;
	omelettium_compilation_append_cflag(&exe, "-O3");
	omelettium_compilation_append_ldflag(&exe, "-pie");
	exe.compiler = "x86_64-w64-mingw32-gcc";
	char *artifact = omelettium_compile(&exe, NULL);

	omelettium_install(artifact, "bin/hello.exe", 0755);
}
