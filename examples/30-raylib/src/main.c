#include <stdio.h>
#include <raylib.h>

int main()
{
	InitWindow(800, 600, "Raylib + Omelettium");
	SetTargetFPS(60);

	while(!WindowShouldClose()) {
		BeginDrawing();
		ClearBackground(RAYWHITE);
		DrawText("This example was built with Omelettium", 10, 10, 30, BLACK);
		EndDrawing();
	}

	CloseWindow();
}
