#include <stdio.h>
#define __OMELETTIUM_H_IMPL
#include "../../omelettium.h"

#define RAYLIB_SOURCES \
X("raylib/src/rcore.c") \
X("raylib/src/rglfw.c") \
X("raylib/src/rmodels.c") \
X("raylib/src/rshapes.c") \
X("raylib/src/rtext.c") \
X("raylib/src/rtextures.c") \
X("raylib/src/utils.c")

int main(int argc, char **argv)
{
	omelettium_init(argc, argv);

	struct omelettium_command git_raylib = omelettium_command_init("git");
	omelettium_command_append_arg(&git_raylib, "clone");
	omelettium_command_append_arg(&git_raylib, "--depth");
	omelettium_command_append_arg(&git_raylib, "1");
	omelettium_command_append_arg(&git_raylib, "https://github.com/raysan5/raylib");
	omelettium_run(&git_raylib);

	struct omelettium_compilation raylib = {0};
	raylib.name = "raylib.a";
	raylib.disable_link = true;
	raylib.disable_werror = true;
	raylib.disable_wall = true;
	raylib.disable_wextra = true;
	raylib.disable_pedantic = true;
	#define X(s) omelettium_compilation_append_src(&raylib, s);
	RAYLIB_SOURCES
	#undef X
	omelettium_compilation_append_cflag(&raylib, "-D_GLFW_X11");
	omelettium_compilation_append_cflag(&raylib, "-DPLATFORM_DESKTOP");
	omelettium_compilation_append_cflag(&raylib, "-std=c99");
	omelettium_compilation_append_cflag(&raylib, "-Iraylib/src/external/glfw/include");
	char *raylib_artifact = omelettium_compile(&raylib, NULL);

	struct omelettium_command raylib_ar = omelettium_command_init("ar");
	omelettium_command_append_arg(&raylib_ar, "rcs");
	omelettium_command_append_arg(&raylib_ar, raylib_artifact);
	#define X(s) omelettium_command_append_arg(&raylib_ar, ".build/" s ".o");
	RAYLIB_SOURCES
	#undef X
	omelettium_run(&raylib_ar);

	struct omelettium_compilation exe = {0};
	exe.name = "raylib-example";
	omelettium_compilation_append_src(&exe, "src/main.c");
	omelettium_compilation_append_cflag(&exe, "-I./raylib/src");
	omelettium_compilation_append_ldflag(&exe, raylib_artifact);
	omelettium_compilation_append_ldflag(&exe, "-lm");
	char *artifact = omelettium_compile(&exe, NULL);

	omelettium_install(artifact, "bin/raylib-example", 0755);
}
