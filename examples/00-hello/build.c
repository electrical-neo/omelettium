#include <stdio.h>
#define __OMELETTIUM_H_IMPL
#include "../../omelettium.h"

int main(int argc, char **argv)
{
	omelettium_init(argc, argv);

	struct omelettium_compilation exe = {0};
	exe.name = "hello";
	omelettium_compilation_append_src(&exe, "src/main.c");
	char *artifact = omelettium_compile(&exe, NULL);

	omelettium_install(artifact, "bin/hello", 0755);
}
