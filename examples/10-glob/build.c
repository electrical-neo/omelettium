#include <stdio.h>
#define __OMELETTIUM_H_IMPL
#include "../../omelettium.h"

int main(int argc, char **argv)
{
	omelettium_init(argc, argv);

	struct omelettium_compilation exe = {0};
	exe.name = "glob";
	omelettium_compilation_append_srcs_from_dir(&exe, "src");
	char *artifact = omelettium_compile(&exe, NULL);

	omelettium_install(artifact, "bin/glob", 0755);
}
