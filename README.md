# Omelettium - a header only build system
## Quick start
Go into any of the examples and run:
```sh
gcc ./build.c -o build
./build
```
The example will be compiled and installed into `out`

## Rebuilding the build script
Once the build script is built, there is no need to manually run the compiler - Omelettium will do it

## Incremental compilation
Omelettium does incremental compilation. When checking if a source file should be recompiled, also header files included by it are checked - if any of them are modified, the whole source file is recompiled.

_Making the mother of all build systems here, Jack._

_Can't fret over every leaked memory._
