#pragma once
#include <errno.h>
#include <ctype.h>
#include <dirent.h>
#include <fcntl.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <unistd.h>

typedef uint8_t u8;
typedef int8_t i8;
typedef uint16_t u16;
typedef int16_t i16;
typedef uint32_t u32;
typedef int32_t i32;
typedef uint64_t u64;
typedef int64_t i64;
typedef size_t usize;

void omelettium_init(int argc, char **argv);
void omelettium_install(char *from, char *dest, mode_t mode);

char *omelettium_shift_args(int *argc, char ***argv);
void omelettium_panic(char *format, ...)
	__attribute__((noreturn))
	__attribute__((format(printf, 1, 2)));
void *omelettium_malloc(usize size);
void *omelettium_realloc(void *m, usize size);
char *omelettium_format(char *format, ...)
	__attribute__((format(printf, 1, 2)));
void omelettium_mkdir_for_file(char *path);
bool omelettium_check_extension(char *name, char *dotext);

struct omelettium_command {
	char **args; // ends with a NULL
	u32 args_count; // NOT including the terminating NULL
	u32 args_cap; // NOT including the terminating NULL

	bool silent;
};

struct omelettium_command omelettium_command_init(char *prog);
void omelettium_command_append_arg(struct omelettium_command *cmd, char *arg);
void omelettium_run(struct omelettium_command *cmd);
pid_t omelettium_run_async(struct omelettium_command *cmd, int *out_stdout_fd);
void omelettium_process_wait(pid_t pid);

struct omelettium_compilation {
	char *name;
	char *artifact_prefix;

	char **srcs;
	u32 srcs_count;
	u32 srcs_cap;

	char *compiler;

	char **cflags;
	u32 cflags_count;
	u32 cflags_cap;

	char **ldflags;
	u32 ldflags_count;
	u32 ldflags_cap;

	bool disable_werror;
	bool disable_wall;
	bool disable_wextra;
	bool disable_pedantic;
	bool disable_link;
};

void omelettium_compilation_append_src(struct omelettium_compilation *comp, char *src);
void omelettium_compilation_append_srcs_from_dir(struct omelettium_compilation *comp, char *dir);
void omelettium_compilation_append_cflag(struct omelettium_compilation *comp, char *flag);
void omelettium_compilation_append_ldflag(struct omelettium_compilation *comp, char *flag);
char *omelettium_compile(struct omelettium_compilation *comp, bool *out_updated);

#ifdef __OMELETTIUM_H_IMPL

char *omelettium_prefix = "out";

void omelettium_init(int argc, char **argv)
{
	char **saved_argv = argv;
	char *self_exe = omelettium_shift_args(&argc, &argv);

	char *cmd;
	while((cmd = omelettium_shift_args(&argc, &argv))) {
		if(!strcmp(cmd, "--prefix")) {
			omelettium_prefix = omelettium_shift_args(&argc, &argv);
			if(!omelettium_prefix)
				omelettium_panic("expected the installation prefix after `--prefix`");

			omelettium_prefix = strdup(omelettium_prefix);
			for(usize i = strlen(omelettium_prefix); omelettium_prefix[i] == '/' && i; i--)
				omelettium_prefix[i] = 0;
		} else if(!strcmp(cmd, "--help")) {
			printf("usage: %s [--help] [--prefix PREFIX]\n", self_exe);
			printf("--help             Print this help message and exit\n");
			printf("--prefix PREFIX    Set the installation prefix (default: out)\n");
			exit(0);
		} else {
			omelettium_panic("invalid command: %s", cmd);
		}
	}

	struct omelettium_compilation comp = {0};
	comp.artifact_prefix = ".";
	comp.name = self_exe;
	omelettium_compilation_append_src(&comp, "build.c");
	bool updated;
	omelettium_compile(&comp, &updated);

	if(updated) {
		execvp(saved_argv[0], saved_argv);
		omelettium_panic("execvp failed: %s", strerror(errno));
	}
}

void omelettium_install(char *from, char *dest, mode_t mode)
{
	char *full_path = omelettium_format("%s/%s", omelettium_prefix, dest);
	omelettium_mkdir_for_file(full_path);

	FILE *ffrom = fopen(from, "rb");
	FILE *fto = fopen(full_path, "wb");

	int b;
	while((b = fgetc(ffrom)) != EOF)
		fputc(b, fto);

	fclose(ffrom);
	fchmod(fileno(fto), mode);
	fclose(fto);
}

char *omelettium_shift_args(int *argc, char ***argv)
{
	if(!*argc)
		return NULL;

	(*argc)--;
	char *arg = **argv;
	(*argv)++;
	return arg;
}

void omelettium_panic(char *format, ...)
{
	va_list ap;
	va_start(ap, format);

	fprintf(stderr, "panic: ");
	vfprintf(stderr, format, ap);
	fputc('\n', stderr);

	va_end(ap);
	exit(1);
}

void *omelettium_malloc(usize size)
{
	void *m = malloc(size);
	if(!m)
		omelettium_panic("out of memory");
	return m;
}

void *omelettium_realloc(void *m, usize size)
{
	void *rm = realloc(m, size);
	if(!rm)
		omelettium_panic("out of memory");
	return rm;
}

char *omelettium_format(char *format, ...)
{
	va_list ap;
	va_start(ap, format);
	char *buf = omelettium_malloc(vsnprintf(NULL, 0, format, ap) + 1);

	va_end(ap);
	va_start(ap, format);
	vsprintf(buf, format, ap);
	va_end(ap);

	return buf;
}

void omelettium_mkdir_for_file(char *path)
{
	char *path_dup = strdup(path);
	if(!path_dup)
		omelettium_panic("out of memory");

	char *token = strtok(path_dup, "/");

	int dir = open(".", O_RDONLY);
	if(dir == -1)
		omelettium_panic("open failed: %s", strerror(errno));

	while(true) {
		char *new_token = strtok(NULL, "/");
		if(!new_token)
			break;

		if(mkdirat(dir, token, 0777) == -1) {
			if(errno != EEXIST) {
				close(dir);
				omelettium_panic("mkdirat failed: %s", strerror(errno));
			}
		}

		int new_dir = openat(dir, token, O_RDONLY);
		close(dir);
		if(new_dir == -1)
			omelettium_panic("openat failed: %s", strerror(errno));

		dir = new_dir;
		token = new_token;
	}

	free(path_dup);
}

bool omelettium_check_extension(char *name, char *dotext)
{
	usize name_len = strlen(name);
	usize dotext_len = strlen(dotext);

	if(name_len < dotext_len)
		return false;

	return !memcmp(name + name_len - dotext_len, dotext, dotext_len);
}

struct omelettium_command omelettium_command_init(char *prog)
{
	struct omelettium_command cmd = {0};
	omelettium_command_append_arg(&cmd, prog);
	return cmd;
}

void omelettium_command_append_arg(struct omelettium_command *cmd, char *arg)
{
	if(cmd->args_count == cmd->args_cap) {
		cmd->args_cap += 128;
		cmd->args = omelettium_realloc(cmd->args, sizeof(*cmd->args) * (cmd->args_cap + 1));
	}

	cmd->args[cmd->args_count] = arg;
	cmd->args[++cmd->args_count] = NULL;
}

void omelettium_command_print(struct omelettium_command *cmd)
{
	if(cmd->silent)
		return;

	fprintf(stderr, "[CMD]");
	for(usize i = 0; i < cmd->args_count; i++)
		fprintf(stderr, " %s", cmd->args[i]);
	fputc('\n', stderr);
}

void omelettium_run(struct omelettium_command *cmd)
{
	pid_t pid = fork();
	if(pid == -1)
		omelettium_panic("fork failed: %s", strerror(errno));

	if(pid) {
		omelettium_process_wait(pid);
	} else {
		omelettium_command_print(cmd);
		execvp(cmd->args[0], cmd->args);
		omelettium_panic("execvp failed: %s", strerror(errno));
	}
}

pid_t omelettium_run_async(struct omelettium_command *cmd, int *out_stdout_fd)
{
	int stdout_pipe[2];
	if(pipe(stdout_pipe))
		omelettium_panic("pipe failed: %s", strerror(errno));

	pid_t pid = fork();
	if(pid == -1)
		omelettium_panic("fork failed: %s", strerror(errno));

	if(pid) {
		close(stdout_pipe[1]);
		*out_stdout_fd = stdout_pipe[0];
	} else {
		close(stdout_pipe[0]);
		if(dup2(stdout_pipe[1], 1) == -1)
			omelettium_panic("dup2 failed: %s", strerror(errno));

		omelettium_command_print(cmd);
		execvp(cmd->args[0], cmd->args);
		omelettium_panic("execvp failed: %s", strerror(errno));
	}

	return pid;
}

void omelettium_process_wait(pid_t pid)
{
	int wstatus;
	if(waitpid(pid, &wstatus, 0) == -1)
		omelettium_panic("wait failed: %s", strerror(errno));

	if(WIFEXITED(wstatus) ? WEXITSTATUS(wstatus) != 0 : false)
		omelettium_panic("child process failed: terminated with code %d", WEXITSTATUS(wstatus));
	else if(WIFSIGNALED(wstatus))
		omelettium_panic("child process failed: terminated by signal %s", strsignal(WTERMSIG(wstatus)));
}

void omelettium_compilation_append_src(struct omelettium_compilation *comp, char *src)
{
	if(comp->srcs_count == comp->srcs_cap) {
		comp->srcs_cap += 128;
		comp->srcs = omelettium_realloc(comp->srcs, sizeof(*comp->srcs) * comp->srcs_cap);
	}

	comp->srcs[comp->srcs_count++] = src;
}

void omelettium_compilation_append_srcs_from_dir(struct omelettium_compilation *comp, char *dir)
{
	DIR *d = opendir(dir);
	if(!d)
		omelettium_panic("cannot open directory `%s`: %s", dir, strerror(errno));

	struct dirent *ent;
	while((ent = readdir(d))) {
		if(!strcmp(ent->d_name, ".") || !strcmp(ent->d_name, ".."))
			continue;

		char *entd = omelettium_format("%s/%s", dir, ent->d_name);
		if(ent->d_type == DT_DIR) {
			omelettium_compilation_append_srcs_from_dir(comp, entd);
		} else if(omelettium_check_extension(ent->d_name, ".c")) {
			omelettium_compilation_append_src(comp, entd);
		}
	}
}

void omelettium_compilation_append_cflag(struct omelettium_compilation *comp, char *flag)
{
	if(comp->cflags_count == comp->cflags_cap) {
		comp->cflags_cap += 128;
		comp->cflags = omelettium_realloc(comp->cflags, sizeof(*comp->cflags) * comp->cflags_cap);
	}

	comp->cflags[comp->cflags_count++] = flag;
}

void omelettium_compilation_append_ldflag(struct omelettium_compilation *comp, char *flag)
{
	if(comp->ldflags_count == comp->ldflags_cap) {
		comp->ldflags_cap += 128;
		comp->ldflags = omelettium_realloc(comp->ldflags, sizeof(*comp->ldflags) * comp->ldflags_cap);
	}

	comp->ldflags[comp->ldflags_count++] = flag;
}

char *omelettium_compile(struct omelettium_compilation *comp, bool *out_updated)
{
	char *compiler = comp->compiler ? comp->compiler : "gcc";

	bool updated;
	if(!out_updated)
		out_updated = &updated;

	*out_updated = false;

	char *artifact = omelettium_format("%s/%s", comp->artifact_prefix ? comp->artifact_prefix : ".build", comp->name);

	struct stat artifact_stat;
	time_t artifact_mtime = stat(artifact, &artifact_stat) == -1 ? 0 : artifact_stat.st_mtime;

	struct omelettium_command cmd = omelettium_command_init(compiler);

	for(u32 i = 0; i < comp->srcs_count; i++) {
		struct omelettium_command lh_cmd = omelettium_command_init(compiler);
		lh_cmd.silent = true;
		omelettium_command_append_arg(&lh_cmd, "-MM");
		omelettium_command_append_arg(&lh_cmd, comp->srcs[i]);
		for(usize i = 0; i < comp->cflags_count; i++)
			omelettium_command_append_arg(&lh_cmd, comp->cflags[i]);

		int pipe;
		pid_t pid = omelettium_run_async(&lh_cmd, &pipe);

		FILE *stream = fdopen(pipe, "r");

		int c;
		while((c = fgetc(stream)) != ':');
		fgetc(stream);

		char path[1024];
		u32 path_len = 0;

		bool cu_updated = false;
		while((c = fgetc(stream)) != EOF) {
			if(isspace(c)) {
				if(!path_len)
					continue;

				path[path_len] = 0;
				path_len = 0;

				struct stat h_stat;
				if(stat(path, &h_stat) == -1)
					omelettium_panic("stat on `%s` failed", comp->srcs[i]);

				if(h_stat.st_mtime >= artifact_mtime) {
					cu_updated = true;
					break;
				}

				continue;
			}

			if(c == '\\')
				continue;

			if(path_len + 1 == sizeof(path))
				omelettium_panic("path too long");

			path[path_len++] = c;
		}

		fclose(stream);
		omelettium_process_wait(pid);

		char *obj_out = omelettium_format(".build/%s.o", comp->srcs[i]);

		if(cu_updated) {
			*out_updated = true;
			omelettium_mkdir_for_file(obj_out);

			struct omelettium_command obj_cmd = omelettium_command_init(compiler);
			if(!comp->disable_werror)
				omelettium_command_append_arg(&obj_cmd, "-Werror");
			if(!comp->disable_wall)
				omelettium_command_append_arg(&obj_cmd, "-Wall");
			if(!comp->disable_wextra)
				omelettium_command_append_arg(&obj_cmd, "-Wextra");
			if(!comp->disable_pedantic)
				omelettium_command_append_arg(&obj_cmd, "-pedantic");

			omelettium_command_append_arg(&obj_cmd, "-c");

			omelettium_command_append_arg(&obj_cmd, "-o");
			omelettium_command_append_arg(&obj_cmd, obj_out);

			omelettium_command_append_arg(&obj_cmd, comp->srcs[i]);

			for(usize i = 0; i < comp->cflags_count; i++)
				omelettium_command_append_arg(&obj_cmd, comp->cflags[i]);

			omelettium_run(&obj_cmd);
		}

		if(!comp->disable_link)
			omelettium_command_append_arg(&cmd, obj_out);
	}

	if(*out_updated && !comp->disable_link) {
		omelettium_command_append_arg(&cmd, "-o");
		omelettium_command_append_arg(&cmd, artifact);

		for(usize i = 0; i < comp->ldflags_count; i++)
			omelettium_command_append_arg(&cmd, comp->ldflags[i]);

		omelettium_run(&cmd);
	}

	return artifact;
}

#endif
